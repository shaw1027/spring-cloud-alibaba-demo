本仓库为笔者关于 Spring Cloud Alibaba 博文的配套代码，可移步访问 [笔者博客](https://blog.csdn.net/shawearn1027) 阅读 [Spring Cloud Alibaba 系列博文](https://blog.csdn.net/shawearn1027/category_10193268.html)

参考链接：

- [Spring Cloud Alibaba 官方示例代码](https://github.com/alibaba/spring-cloud-alibaba/tree/master/spring-cloud-alibaba-examples)

