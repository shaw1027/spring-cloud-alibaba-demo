package com.shawearn.spring.cloud.alibaba.dto;

import java.io.Serializable;

/**
 * 本示例中进行 Dubbo 调用时需要传递的参数；
 * 需要继承 java.io.Serializable 接口；
 */
public class UserInfoDto implements Serializable {
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
