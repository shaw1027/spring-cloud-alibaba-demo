package com.shawearn.spring.cloud.alibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

//@EnableDiscoveryClient
@SpringBootApplication
public class NacosDubboConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(NacosDubboConsumerApplication.class, args);
	}

}
