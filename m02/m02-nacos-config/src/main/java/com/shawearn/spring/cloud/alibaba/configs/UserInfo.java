package com.shawearn.spring.cloud.alibaba.configs;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
@ConfigurationProperties(prefix = "user-info")
public class UserInfo implements Serializable {

    private String name;

    private String blogAddr;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBlogAddr() {
        return blogAddr;
    }

    public void setBlogAddr(String blogAddr) {
        this.blogAddr = blogAddr;
    }
}
