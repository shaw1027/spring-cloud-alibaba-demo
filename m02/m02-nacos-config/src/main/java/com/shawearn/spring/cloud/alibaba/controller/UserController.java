package com.shawearn.spring.cloud.alibaba.controller;

import com.shawearn.spring.cloud.alibaba.configs.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserInfo userInfo;

    @RequestMapping("/userInfo")
    public UserInfo userInfo() {
        return userInfo;
    }

}
