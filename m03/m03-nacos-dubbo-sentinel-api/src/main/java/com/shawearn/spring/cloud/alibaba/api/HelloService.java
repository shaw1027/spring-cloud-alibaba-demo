package com.shawearn.spring.cloud.alibaba.api;

import com.shawearn.spring.cloud.alibaba.dto.UserInfoDto;

/**
 * 定义 Dubbo 服务接口；
 */
public interface HelloService {

    /**
     * Dubbo 服务接口；
     *
     * @param userInfo
     * @return
     */
    String sayHello(UserInfoDto userInfo);

}
