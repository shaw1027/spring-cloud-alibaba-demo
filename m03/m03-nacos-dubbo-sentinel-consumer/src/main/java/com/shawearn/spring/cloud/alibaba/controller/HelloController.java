package com.shawearn.spring.cloud.alibaba.controller;

import com.shawearn.spring.cloud.alibaba.api.HelloService;
import com.shawearn.spring.cloud.alibaba.dto.UserInfoDto;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HelloController {

    @org.apache.dubbo.config.annotation.Reference
    private HelloService helloService;

    @RequestMapping("/hello")
    public Map sayHello(@RequestParam String name) {
        // 组装请求参数；
        UserInfoDto userInfoDto = new UserInfoDto();
        userInfoDto.setName(name);
        userInfoDto.setId(userInfoDto.hashCode());

        // 调用远程接口；
        String s = helloService.sayHello(userInfoDto);

        // 封装返回结果参数；
        Map result = new HashMap();
        result.put("code", 0);
        result.put("msg", "请求成功");
        result.put("result", s);
        return result;
    }
}
