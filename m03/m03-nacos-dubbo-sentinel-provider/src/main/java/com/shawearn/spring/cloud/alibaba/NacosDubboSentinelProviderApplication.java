package com.shawearn.spring.cloud.alibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NacosDubboSentinelProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(NacosDubboSentinelProviderApplication.class, args);
	}

}
