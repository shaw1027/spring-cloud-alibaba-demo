package com.shawearn.spring.cloud.alibaba.provider;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.shawearn.spring.cloud.alibaba.api.HelloService;
import com.shawearn.spring.cloud.alibaba.dto.UserInfoDto;

/**
 * Dubbo 服务接口的实现类；
 */
@org.apache.dubbo.config.annotation.Service
public class HelloServiceProvider implements HelloService {

    @Override
//    @SentinelResource(value = "sayHello")
    public String sayHello(UserInfoDto userInfo) {
        String str = "%s，你好，你的用户 ID 是 %d";
        return String.format(str, userInfo.getName(), userInfo.getId());
    }
}
